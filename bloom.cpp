#include <cstdlib>
#include <iostream>

using namespace std;

// g++ bloom.cpp -o bloom
// pgc++ bloom.cpp -o bloom-gpu -ta=tesla

class bloom {

	public:

	//Array of 32-bit values
	//These are saturating counters
	int* array;
	
	//Length of the array
	long long int size;

	//Number of hashes; more is slower but more accurate
	int hashes;

	//Constant table of random 64-bit numbers
	//Used as bitmasks for hashing
	long long int randomTable[8*256];

	//Constant table of 8-bit reverse-complements
	long long int rcompTable[256];

	//Constructor
	//Size should ideally be prime
	bloom(int hashes_, long long int size_){
		hashes=hashes_;
		size=(size_|1LL);
		array=(int *)malloc(4*size);
		for(int i=0;i<size;i++) array[i]=0; 
		fillRandomTable();
		fillRcompTable();
		
	}

	//Perform a lookup of a kmer
	//This returns the minimum of N lookups
	//where N is the fixed hashes value.
	#pragma acc routine seq
	int lookup(long long int key){
		//cout << "Lookup " << key << endl;
		
		//key=canonize(key);
		int r=0x7FFFFFFF;
		long long int hashcode=key;
		for(int i=0; i<hashes && r>0; i++){
			int value=lookupHashed(hashcode);
			r=min(r, value);
			//cout << "Loop " << i << ": value=" << value << ", r=" << r << endl;
			hashcode=hash(hashcode+i);
		}
		return r;
	}
	
	//Perform lookup of a hashcode
	#pragma acc routine seq
	int lookupHashed(long long int hashcode){
		int index=hashcode%size;
		//cout << "hash = " << hashcode << ", index = " << index << ", array[index] = " << array[index] << endl;
		return array[index];
	}
	
	//Generate a 63-bit hashcode for an input 64-bit number
	#pragma acc routine seq
	long long int hash(long long int number){
		long long int code=number;
		for(int i=0; i<8; i++){
			//1-D notation for long[8][256] matrix
			long long int mask=randomTable[i*256+number&0xFF];
			code=code^mask;
			number>>=8;
		}
		return code&0x7FFFFFFFFFFFFFFFLL;
	}

	//increment the count for a kmer
	#pragma acc routine seq
	void increment(long long key){
		//cout << "Increment " << key << endl;
		//key=canonize(key);
		long long int hashcode=key;
		for(int i=0; i<hashes; i++){
			incrementHashed(hashcode);
			hashcode=hash(hashcode+i);
		}
	}

	//This should be an atomic operation!
	#pragma acc routine seq 
	void incrementHashed(long long int hashcode){
		int index=hashcode%size;

		// prevent overflow
		

		//int old=array[index];
		//int result=(old+1);
		
		int result;
		#pragma acc atomic capture
		{
			//array[index]=array[index]+1;
			result = array[index]++;
			//result = array[index]++;
		}
		
		if(result<0){
			result=0x7FFFFFFF;
			#pragma acc atomic write
			array[index]=result;
		}
		
		//array[index]=result;

		
		//cout << "hash = " << hashcode << ", index = " << index << ", array[index] = " << array[index] << endl;
	}

	//Random 64-bit number
	//Ripped from:
	//https://stackoverflow.com/questions/3665257/generate-random-long-number
	long long int lrand(){
		if (sizeof(int) < sizeof(long)){
			return (static_cast<long>(rand()) << (sizeof(int) * 8)) |
			rand();
		}
		return rand();
	}

	#pragma acc routine seq
	int min(int a, int b){
		return a < b ? a : b;
	}

	#pragma acc routine seq
	int max(int a, int b){
		return a > b ? a : b;
	}

	//Return a canonical version of this kmer or its reverse-complement
	#pragma acc routine seq
	long long int canonize(long long int kmer, int k){
		long long int rkmer=rcompFast(kmer, k);
		return max(kmer, rkmer);
	}

	//Fast reverse-complement, 8 bits per cycle via lookup
	#pragma acc routine seq
	long long int rcompFast(long long int kmer, int k){
		long long int rkmer=0;
		int extra=k&3;

		//First non-mult-of-4 bases
		for(int i=0; i<extra; i++){
			rkmer=((rkmer<<2)|((~kmer)&3L));
			kmer>>=2;
		}
		k-=extra;

		//K is now a multiple of 4
		for(int i=0; i<k; i+=4){
			rkmer=((rkmer<<8)|(rcompTable[(int)(kmer&0xFFL)]));
			kmer>>=8;
		}
		return rkmer;
	}

	//Slow reverse-complement
	//Only use this for making the initial lookup table
	#pragma acc routine seq
	long long int rcomp(long long int kmer, int k){
		long long int rkmer=0;
		int cycles=2*k;
		kmer=~kmer; //complement
		for(int i=0; i<cycles; i++){ //reverse
			rkmer=(rkmer<<2)|(kmer&3);
			kmer>>=2;
		}
		return rkmer;
	}
	
	//fill randomTable with random numbers at init
	void fillRandomTable(){
		int lim=8*256;
		for(int i=0; i<lim; i++){
			randomTable[i]=lrand();
		}
	}
	
	//fill rcompTable with 8-bit reverse-complements at init
	void fillRcompTable(){
		int lim=256;
		for(int i=0; i<lim; i++){
			rcompTable[i]=rcomp(i, 4);
		}
	}
};

//main goes OUTSIDE of object
int main(int argc, char** argv) {


	//Create a Bloom filter
	bloom b(3, 10000000); // 3 = number of hashes, 100 = size of hashes
	
	int n=20000000; //20m
   	
	// atomic-tize me
	#pragma acc parallel loop	
	for(int i=1; i<=n; i+=3){
		b.increment(i);
	}
	
	long sum=0;

	#pragma acc parallel loop	
	for(int i=1;i<=n;i++){
		if(b.lookup(i) > 0){
			//cout << "seen " << i << endl;
			sum += b.lookup(i);
			//sum += i;
		}
	}
	cout << "sum = " << sum << endl;
	
	return 0;
}
