/*
 * Mimic BBTool's seal.sh (kmer based matching)
 *
 *
 * compile: g++ hm.cpp -o hm
 * http://www.cplusplus.com/reference/
 hello folks! 11:29am
    
 */

#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <bitset>
#include <malloc.h>

#define SEP "--------------------------------------------\n"
const bool debug = false; // show lots of debug or not
const int KMER_SIZE = 31; //5;

//signed char kmer_lookup[128]; // = (signed char*); // malloc(128*sizeof(signed char));
signed long long int kmer_lookup[128]; // = (signed char*); // malloc(128*sizeof(signed char));


using namespace std;

// prototypes - forward declarations

int read_fasta(string, map<unsigned long long int, int> &tmp_map, string); 
int count_kmers(string, map<unsigned long long int, int> &tmp_map);
int load_kmers(string, map<unsigned long long int, int> &tmp_map);
unsigned long long int kmerConvert(string);
unsigned long long int kmerConvert2(string);
int testKmerConvert();
string printKmerInBinary(unsigned int);
int kmerConvertMain();

/*
 *
 * MAIN, first function!  Lets go!
 *
 */ 
int main(int argc, char **argv)
{

  kmer_lookup['A'] = 0;
  kmer_lookup['a'] = 0;
  kmer_lookup['C'] = 1;
  kmer_lookup['c'] = 1;
  kmer_lookup['G'] = 2;
  kmer_lookup['g'] = 2;
  kmer_lookup['T'] = 3;
  kmer_lookup['t'] = 3;
  kmer_lookup['N'] = -1;
  kmer_lookup['n'] = -1;

  /*
  cout << "- arr a " << kmer_lookup['A'] << endl;
  cout << "- arr c " << kmer_lookup['C'] << endl;
  cout << "- arr g " << kmer_lookup['G'] << endl;
  cout << "- arr t " << kmer_lookup['T'] << endl;
  */
  
  //printf("argc = %d\n", argc);

  //GATC = 10 00 11 01 = 141
  //CTAG = 01 11 00 10 = 114
  // 2 = 01
  int x = 141;
  printf("%d | %d = %d\n", x, 2, x | 2); // 141 | 2 = 143 = cool
  int y = x << 2; // shift 2 places to the left
  printf("y = %d\n", y); // 564? yes
  printf("Kmer size: %d\n", KMER_SIZE);
  string ref_file, query_file;
  int ref_base_cnt = 0;
  
  map<unsigned long long int, int> ref_map; // hash
  
  
  printf(SEP);
  

  /*
  string kmer = "CCGCGCGCAAGAACTGGTTCTGGGGCAACGC"; // 01011001100110010000100000011110101111011110101010010000011001 = 1614050117235155993
  unsigned long long int kmer_val = 0;
  kmer_val = kmerConvert(kmer);
  cout << "Kmer = " << kmer << " = " << kmer_val << " == " << std::bitset<64>(kmer_val) << endl;
  return 0;
  */
  ref_file = "/ccsopen/proj/gen123/bbtools/data/GHNYP.fasta";
  //ref_file = "/ccsopen/proj/gen123/bbtools/data/GHSUS.fasta";
  //ref_file = "/global/projectb/scratch/brycef/gpu/GHNYA.fasta";
  //ref_file = "/global/projectb/scratch/brycef/gpu/GHNXX.fasta";
  //ref_file = "r.fa";
  
  


  query_file = "/ccsopen/proj/gen123/bbtools/data/GHHPH.fasta";
  //query_file = "/ccsopen/proj/gen123/bbtools/data/GHSUS.3k.fasta";
  //query_file = "/global/projectb/scratch/brycef/gpu/GHNYC.fasta";
  //query_file = "q.fa";
  
  
  ref_base_cnt = read_fasta(ref_file, ref_map, "ref");


  // load query file
  printf(SEP);
  int kmer_cnt = 0, base_match = 0;
  base_match = read_fasta(query_file, ref_map, "query");
  //printf("- matching kmers: %d\n", kmer_cnt);
  
  
  printf(SEP);
  

  return 0;
}

/*
 * process reference
 * read in each contig
 * send to kmer-function to split into kmers and map into our reference hashmap
 *
 */
int read_fasta(string ref_file, map<unsigned long long int, int> &tmp_map, string file_type="ref")
{
  
  string line;
  string bases = "";
  string last_header = "";
  int base_cnt = 0, contig_cnt = 0;
  int kmer_cnt = 0, base_match = 0;
  
  cout << "Loading " << file_type << ": " <<  ref_file << "...\n";
  cout << "\n";
  
  
  
  ifstream myfile(ref_file.c_str());
  
  if (myfile.is_open())
  {
    while (getline(myfile, line))
    {
      
      if(line.at(0) == '>') // single '' for char
      {
        
        if(!last_header.empty())
        {

          printf("contig: %d = %s\n", contig_cnt, last_header.c_str());
          printf("- base count: %d\n", bases.length());

          // kmer-ize it
          // matching base count needed here
          if(file_type == "ref")
          {
            base_match += load_kmers(bases, tmp_map);
          }
          else
          {
            base_match += count_kmers(bases, tmp_map);
          }

        }
        
        contig_cnt++;
        base_cnt += bases.length();
        
        bases = "";
        last_header = line;
        
      }
      else
      {
        // strcpy? strcat?
        bases = bases + line;
      }

    }
    myfile.close();
  }
  else cout << "Unable to open file" << endl;   
  
  // handle last of bases
  base_cnt += bases.length();
  
  if(debug)
  {
    printf("last contig: %d = %s\n", contig_cnt, last_header.c_str());
    printf("- base count: %d\n", bases.length());
    printf("------------------------------------------------\n");
  }
  
  
  //base_match += split_kmers(bases, tmp_map, file_type);
  if(file_type == "ref")
  {
    base_match += load_kmers(bases, tmp_map);
  }
  else
  {
    base_match += count_kmers(bases, tmp_map);
  }
  
  
  
  printf("- total contig count: %d\n", contig_cnt);
  printf("-   total base count: %d\n", base_cnt);
  
  if(file_type != "ref")
  {
    printf(SEP);
    float pct = 0.0;
    pct = 100 * base_match / (float) base_cnt; // cast one to float so percent comes out correctly
    
    printf("- Total base match: %d (%0.2f%%)\n", base_match, pct);
  }
  
  return base_match;

}



/*
 *  split bases into kmers
 *  len = 3166, max kmers = 3135
 *  Brian's substitution - not parallel yet
 */
int count_kmers(string bases, map<unsigned long long int, int> &tmp_map)
{
  int max_kmers = 0;
  int kmer_cnt = 0; // number of matching kmers
  int base_match = 0, base_cnt = 0; // number of matching bases (if kmer exists in ref)
  max_kmers = bases.length() - KMER_SIZE + 1;
  int blen=bases.length();
  //unsigned long long int kmer_val = 0;
  int last_pos = 0;
  
  // want at least 1 kmer with 31 kmers
  if (max_kmers <= 0)
  {
    return 0;
  }
 
  string kmer = "";
  
  long long int mask = 0x7FFFFFFFFFFFFFFF; // prevent from becoming negative (-1 right shifted by 2)
  
  #pragma acc parallel loop
  //for(int thread=0; thread < ((blen +9) / 10); thread++)
  
  {
    long long int kmer_val = 0;
    int len=0; // every thread has private copy of length
    
    //for(int pos=thread*10; pos < blen && pos < thread * 10 + KMER_SIZE; pos++)
    for(int pos=0; pos < blen; pos++)
    {
    
      base_cnt = pos; //base_cnt++;
      int base = bases[pos];
      long long int num=kmer_lookup[base];
      kmer_val = kmer_val << 2;
      kmer_val = kmer_val | num;
      kmer_val &= mask;


      if(num>=0)
      {
        len++;
      } else {
        len=0;
      }
   
      if(len>=KMER_SIZE)
      {
        
        if (tmp_map.find(kmer_val) != tmp_map.end())
        {
           kmer_cnt ++;
           // first kmer found?  (base_match = 0?)
           if(base_match == 0)
           {
             base_match = KMER_SIZE;
           }
           else
           {
             int bm = min(KMER_SIZE, pos - last_pos); // min built into C
             
             base_match += bm;
           }
           last_pos = pos;
         
        }
    
      }

    }
    
  }
  
  
  printf("- total base count: %d\n", base_cnt);
  
  float pct = 0.0;
  pct = 100 * base_match / (float) base_cnt; // cast one to float so percent comes out correctly
  
  printf("- Total base match: %d (%0.2f%%)\n", base_match, pct);
  printf(SEP);
  
  return base_match;

}


/*
 *  split bases into kmers
 *  len = 3166, max kmers = 3135 
 */
int load_kmers(string bases, map<unsigned long long int, int> &tmp_map)
{
  int max_kmers = 0;
  int kmer_cnt = 0; // number of matching kmers
  int base_match = 0, base_cnt = 0; // number of matching bases (if kmer exists in ref)
  max_kmers = bases.length() - KMER_SIZE + 1;
  unsigned long long int kmer_val = 0;
  int last_pos = 0;
  long long int mask = 0x7FFFFFFFFFFFFFFF; // prevent from becoming negative
  
  // want at least 1 kmer with 31 kmers
  if (max_kmers <= 0)
  {
    return 0;
  }
  
  string kmer = "";
  
  /*
  if(debug)
  {
    printf("* bases: %s\n", bases.c_str());
  }
  */
  
  // do hash map update, atomic operation
  int len=0; 
  for(int pos=0; pos < max_kmers; pos++)
  {
    base_cnt++;
    kmer = bases.substr(pos, KMER_SIZE);
    
    int base = bases[pos];
    long long int num=kmer_lookup[base];
    kmer_val = kmer_val << 2;
    kmer_val = kmer_val | num;
    kmer_val &= mask;
    if(num>=0)
    {
      len++;
    } else {
      len=0;
    }
   
    if(len>=KMER_SIZE)
    {
      tmp_map[kmer_val]++;
    }
    
    // debugging
    if (debug)
    {
      string match = "";
      if (tmp_map.find(kmer_val) != tmp_map.end()) match = " *";
      cout << "** kmer " << pos << ":" << kmer.c_str() << " = " << std::bitset<64>(kmer_val) << " = " << kmer_val << match << endl;
    }
    
  }

  printf("- unique kmer count: %d\n", tmp_map.size());
  
  return base_match;
}


unsigned long long int kmerConvert(string kmer)
{
	int kmerLen = 0;
	
  unsigned long long int kmerIntVal = 0;
	
	char key;

	kmerLen = kmer.length();
	if (kmerLen <= KMER_SIZE)
	{
		
		for (int i=0;  i<kmerLen; i++) {
			key=kmer[i];
			
		  kmerIntVal = kmerIntVal << 2;
      kmerIntVal = kmerIntVal | kmer_lookup[key];
      
      //cout << "-- key = " << key << ", kiv = " << kmerIntVal << " == " << std::bitset<64>(kmerIntVal)<< endl;
    }
  }
  return kmerIntVal;

}


/*
 * convert ref string to integer value using a=0 c=1 t=2 g=3
 * process input string kmer less then 32 characters
 * return 
 *    interger value of string  
 *    -1 if string is > 31 characters
 *
 */
unsigned long long int kmerConvert2(string kmer)
{
	int kmerLen = 0;
	//unsigned int kmerIntVal = 0;
  unsigned long long int kmerIntVal = 0;
	
	// entropy of this kmer?
  // gc content of this kmer?
  
	char key;

	kmerLen = kmer.length();
	if (kmerLen <= KMER_SIZE)
	{
		
		for (int i=0;  i<kmerLen; i++) {
			key=kmer[i];
			
		  kmerIntVal = kmerIntVal << 2;

			switch (key) {
				case 'a':
          kmerIntVal = kmerIntVal | 0;
					break;
				case 'c':
          kmerIntVal = kmerIntVal | 1;  // might be faster
					//kmerIntVal += 1;
					break;
				case 't':
					//kmerIntVal += 2;
          kmerIntVal = kmerIntVal | 3;
					break;
				case 'g':
					//kmerIntVal += 3;
          kmerIntVal = kmerIntVal | 2;
					break;
				case 'A':
          kmerIntVal = kmerIntVal | 0;
					break;
				case 'C':
					//kmerIntVal += 1;
          kmerIntVal = kmerIntVal | 1;  // might be faster
					break;
				case 'T':
					//kmerIntVal += 2;
          kmerIntVal = kmerIntVal | 3;
					break;
				case 'G':
					//kmerIntVal += 3;
          kmerIntVal = kmerIntVal | 2;
					break;				
				default:
					break;
			}
		}
   } else 
	 kmerIntVal = -1;
	
  
   // todo: mask middle of kmer (make "A")
   return kmerIntVal;
}

/*
 * calculate binary string based on integer value
 * in: kmer integer value
 * out: string representing bit map of kmer integer value
 *
 */
string printKmerInBinary(unsigned int kmerIntVal) {
	std::string binary = std::bitset<32>(kmerIntVal).to_string() ;
	return binary;
}


/*
 * call kmer convert tests
 */
int kmerConvertMain()
{
	cout << "good morning blackberry" << "\n";
	int pass = testKmerConvert();
	cout << "O & O" << endl;
}

/*
 * test the kmerConvert function
 * sends various reference strings to kmerConvert and verifies integer values returned are correct
 * output:
 *    fail plus teset string that failes
 *    total number of tests that passed     
 *
 */
int testKmerConvert() {
	
	// TODO: c++ unit test framework
	
	unsigned int kmerIntVal = 0;
	int pass = 0; 
	
	string test1 = "a"; //0
	unsigned int test1Result = 0;
	
	string test2 = "c"; //1
	unsigned int test2Result = 1;

	string test3 = "t"; // 3
	unsigned int test3Result = 3;
	
	string test4 = "g"; // 2
	unsigned int test4Result = 2;
	
	string test5 = "aaaa"; // 0
	unsigned int test5Result = 0;
	
	string test6 = "gggg"; // 255
	unsigned int test6Result = 255;
	
	string test7 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"; //0
	unsigned int test7Result = 0;
	
	string test8 = "ggggggggggggggggggggggggggggggg"; //4294967295
	unsigned int test8Result = 4294967295;
	
	string test9 = "ccccccccccccccccccccccccccccccc"; //1431655765
	unsigned int test9Result = 1431655765;
	
	string test10 ="ttttttttttttttttttttttttttttttt"; //2863311530
	unsigned int test10Result = 2863311530;
		
	string test11 = "actg"; //27
	unsigned int test11Result = 27;
	
	string test12 = "aaag"; // 3
	unsigned int test12Result = 3;
	
	string test13 = "A"; //0
	unsigned int test13Result = 0;
	
	string test14 = "C"; //1
	unsigned int test14Result = 1;

	string test15 = "T"; // 3
	unsigned int test15Result = 3;
	
	string test16 = "G"; // 2
	unsigned int test16Result = 2;

	string test17 = "AcTg"; //27
	unsigned int test17Result = 27;

	string test18 = "aCtG"; //27
	unsigned int test18Result = 27;
	

  //"CCGCGCGCAAGAACTGGTTCTGGGGCAACGC" # 01011001100110010000100000011110101111011110101010010000011001 = 1614050117235155993
	

  
	cout << "Starting the test(s) ..." << endl;
	
	kmerIntVal = kmerConvert(test1);
	if (kmerIntVal != test1Result ) cout << "fail: test string is: " << test1 << endl; else pass++;

	kmerIntVal = kmerConvert(test2);
	if (kmerIntVal != test2Result ) cout << "fail: test string is: " << test2 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test3);
	if (kmerIntVal != test3Result ) cout << "fail: test string is: " << test3 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test4);
	if (kmerIntVal != test4Result ) cout << "fail: test string is: " << test4 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test5);
	if (kmerIntVal != test5Result ) cout << "fail: test string is: " << test5 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test6);
	if (kmerIntVal != test6Result ) cout << "fail: test string is: " << test6 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test7);
	if (kmerIntVal != test7Result ) cout << "fail: test string is: " << test7 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test8);
	if (kmerIntVal != test8Result ) cout << "fail: test string is: " << test8 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test9);
	if (kmerIntVal != test9Result ) cout << "fail: test string is: " << test9 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test10);
	if (kmerIntVal != test10Result ) cout << "fail: test string is: " << test10 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test11);
	if (kmerIntVal != test11Result ) cout << "fail: test string is: " << test11 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test12);
	if (kmerIntVal != test12Result ) cout << "fail: test string is: " << test12 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test13);
	if (kmerIntVal != test13Result ) cout << "fail: test string is: " << test13 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test14);
	if (kmerIntVal != test14Result ) cout << "fail: test string is: " << test14 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test15);
	if (kmerIntVal != test15Result ) cout << "fail: test string is: " << test15 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test16);
	if (kmerIntVal != test16Result ) cout << "fail: test string is: " << test16 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test17);
	if (kmerIntVal != test17Result ) cout << "fail: test string is: " << test17 << endl; else pass++;
	
	kmerIntVal = kmerConvert(test18);
	if (kmerIntVal != test18Result ) cout << "fail: test string is: " << test18 << endl; else pass++;
	
  /*
	kmerIntVal = kmerConvert(test19);
	if (kmerIntVal != test19Result ) cout << "fail: test string is: "<< kmerIntVal << test19 << endl; else pass++;

	kmerIntVal = kmerConvert(test20);
	if (kmerIntVal != test20Result ) cout << "fail: test string is: "<< kmerIntVal << test20 << endl; else pass++;
  */
	cout << "tests passed: " << pass << endl;
	
	return (1);

}

/*
 *misc testing code
 *
   
  // testing our map
  
  //printf("-------------------------------------------------\n");
  
  //printf("ref_map[41] = %d\n", ref_map[3419819951718374824]); // hurrah! is it 1!!
  //printf("ref_map unique key count = %d\n", ref_map.size());
  
  
  unsigned long long int k = 0;
  string kmer = "CCGCGCGCAAGAACTGGTTCTGGGGCAACGC";
  k = kmerConvert(kmer);
  cout << "kmer test 2 = " << kmer << " = " << std::bitset<64>(k) << " = " << k << endl;
  unsigned long long int v = 1614050117235155993;
  // CCGCGCGCAAGAACTGGTTCTGGGGCAACGC =
  //   01011001100110010000100000011110101111011110101010010000011001
  // 0001011001100110010000100000011110101111011110101010010000011001
  cout << "kmer test = " << v << " = " << std::bitset<64>(v) << endl;
  
  
	// Create a map iterator and point to beginning of map
  map<unsigned long long int, int>::iterator it = ref_map.begin();
	while (it != ref_map.end())
  {
    cout << "- " << it->first << " :: " << it->second << endl;
    it++;
  }

	
  // goodmorning blackberry???
  //kmerConvertMain();
  
  kmer code
~/git/jgi-rqc-pipeline/tools/kviz -r /global/projectb/scratch/brycef/gpu/GHNYP.fasta -q /global/projectb/scratch/brycef/GHHPH.fasta
*/
