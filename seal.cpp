/*
 * Mimic BBTool's seal.sh (kmer based matching)
 *
 *
 * compile: g++ seal.cpp -o seal
 * 
 * http://www.cplusplus.com/reference/
 */

#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <bitset>


#define SEP "--------------------------------------------\n"
const bool debug = false; // show lots of debug or not

const int KMER_SIZE = 31;
//const long long int MASK = 0x7FFFFFFFFFFFFFFF; // prevent from becoming negative (-1 right shifted by 2)

//const int KMER_SIZE = 5; 
//const long long int MASK = 1023;
//int kmer_list[1024];
//const int KMER_SIZE = 15;
//const long long int MASK = 1073741823;
//int kmer_list[1073741823];


signed long long int kmer_lookup[128]; // = (signed char*); // malloc(128*sizeof(signed char));


using namespace std;

// prototypes - forward declarations

int read_fasta(string, map<unsigned long long int, int> &tmp_map, string); 
int count_kmers(string, map<unsigned long long int, int> &tmp_map);
int load_kmers(string, map<unsigned long long int, int> &tmp_map);
long long int rcomp(long long int);


/*
 *
 * MAIN, first function!  Lets go!
 *
 */ 
int main(int argc, char **argv)
{

  if(debug)
  {
    signed long long int mask = ~((-1LL) << (2*KMER_SIZE));
    cout << "kmer_size = " << KMER_SIZE << ", bit mask = " << bitset<64>(mask) << endl;
    cout << "rcomp bit mask = " << bitset<64>(rcomp(mask)) << endl;
  }
  //return 0;

  // lookup table
  kmer_lookup['A'] = 0;
  kmer_lookup['a'] = 0;
  kmer_lookup['C'] = 1;
  kmer_lookup['c'] = 1;
  kmer_lookup['G'] = 2;
  kmer_lookup['g'] = 2;
  kmer_lookup['T'] = 3;
  kmer_lookup['t'] = 3;
  kmer_lookup['N'] = -1;
  kmer_lookup['n'] = -1;

  string ref_file, query_file;
  int ref_base_cnt = 0;
  
  map<unsigned long long int, int> ref_map; // hash
  
  
  printf(SEP);
  
  /*
  string kmer = "CCGCGCGCAAGAACTGGTTCTGGGGCAACGC"; // 01011001100110010000100000011110101111011110101010010000011001 = 1614050117235155993
  unsigned long long int kmer_val = 0;
  kmer_val = kmerConvert(kmer);
  cout << "Kmer = " << kmer << " = " << kmer_val << " == " << std::bitset<64>(kmer_val) << endl;
  return 0;
  */
  //ref_file = "/ccsopen/proj/gen123/bbtools/data/GHNYP.fasta";
  //ref_file = "/ccsopen/proj/gen123/bbtools/data/GHHPH.fasta";
  //ref_file = "/ccsopen/proj/gen123/bbtools/data/GHSUS.fasta";
  //ref_file = "/global/projectb/scratch/brycef/gpu/GHNYA.fasta";

  ref_file = "/ccsopen/proj/gen123/bbtools/data/GHNXX.fasta";
  //ref_file = "/ccsopen/proj/gen123/bbtools/data/test.fasta";
  //ref_file = "r.fa";
  
  


  query_file = "/ccsopen/proj/gen123/bbtools/data/GHHPH.fasta";
  //query_file = "/ccsopen/proj/gen123/bbtools/data/testq.fasta";
  //query_file = "/ccsopen/proj/gen123/bbtools/data/GHSUS.3k.fasta";
  //query_file = "/global/projectb/scratch/brycef/gpu/GHNYC.fasta";
  //query_file = "q.fa";
  
  
  ref_base_cnt = read_fasta(ref_file, ref_map, "ref");
  

  // load query file
  printf(SEP);
  printf("\n");
  int kmer_cnt = 0, base_match = 0;
  base_match = read_fasta(query_file, ref_map, "query");
  
  
  printf(SEP);
  

  return 0;
}

/*
 * process reference
 * read in each contig
 * send to kmer-function to split into kmers and map into our reference hashmap
 *
 */
int read_fasta(string ref_file, map<unsigned long long int, int> &tmp_map, string file_type="ref")
{
  
  string line;
  string bases = "";
  
  string last_header = "";
  int base_cnt = 0, contig_cnt = 0;
  int kmer_cnt = 0, base_match = 0;
  
  cout << "Loading " << file_type << ": " <<  ref_file << "...\n";
  cout << "\n";
  
  
  
  ifstream myfile(ref_file.c_str());
  
  if (myfile.is_open())
  {
    while (getline(myfile, line))
    {
      
      if(line.at(0) == '>') // single '' for char
      {
        
        if(!last_header.empty())
        {

          printf("contig: %d = %s\n", contig_cnt, last_header.c_str());
          //printf("- base count: %d\n", bases.length());

          // kmer-ize it
          // matching base count needed here
          if(file_type == "ref")
          {
            kmer_cnt += load_kmers(bases, tmp_map);
          }
          else
          {
            base_match += count_kmers(bases, tmp_map);
          }

        }
        
        contig_cnt++;
        base_cnt += bases.length();
        
        bases = "";
        last_header = line;
        
      }
      else
      {
        // strcpy? strcat?
        bases = bases + line;
      }

    }
    myfile.close();
  }
  else cout << "Unable to open file" << endl;   
  
  // handle last of bases
  base_cnt += bases.length();
  

  printf("last contig: %d = %s\n", contig_cnt, last_header.c_str());
  //printf("- base count: %d\n", bases.length());

  if(file_type == "ref")
  {
    base_match += load_kmers(bases, tmp_map);
  }
  else
  {
    base_match += count_kmers(bases, tmp_map);
  }
  
  
  printf("\n");
  printf("- total contig count: %d\n", contig_cnt);
  printf("-   total base count: %d\n", base_cnt);
  
  if(file_type != "ref")
  {
    printf(SEP);
    float pct = 0.0;
    pct = 100 * base_match / (float) base_cnt; // cast one to float so percent comes out correctly
    
    printf("- Total bases match: %d (%0.2f%%)\n", base_match, pct);
  }
  
  return base_match;

}



/*
 *  Count how many kmers from bases are in tmp_map
 *  
 *  
 */
int count_kmers(string bases, map<unsigned long long int, int> &tmp_map)
{
  int max_kmers = 0;
  int base_cnt = 0; // number of matching bases (if kmer exists in ref)
  int blen=bases.length();
  int last_pos = 0;
  int len=0; // every thread has private copy of length
  signed long long int mask = ~((-1LL) << 2*KMER_SIZE);
  char *cbases = const_cast<char*>(bases.c_str());
  int kmer_cnt = 0; // number of matching kmers
  int base_match = 0;
  long long int kmer_val = 0, rkmer_val = 0;
  int ttl_kmer_cnt = 0;
  
  // want at least 1 kmer with 31 kmers
  max_kmers = bases.length() - KMER_SIZE + 1;
  if (max_kmers <= 0)
  {
    return 0;
  }
  

  for(int pos=0;pos < blen;pos++)
  {
    
    base_cnt++;
    int base = cbases[pos];
    long long int num=kmer_lookup[base];
    kmer_val = kmer_val << 2;
    kmer_val = kmer_val | num;
    kmer_val &= mask;

    num >= 0 ? len++ : len = 0;
 
    if(len>=KMER_SIZE)
    {
      rkmer_val = rcomp(kmer_val);
      kmer_val > rkmer_val ? kmer_val : rkmer_val;
      if (debug)
      {
        cout << "** kmer " << pos << ": " << bitset<64>(kmer_val) << " = " << kmer_val << endl;
      }
      ttl_kmer_cnt++;
      if (tmp_map.find(kmer_val) != tmp_map.end())
      {
         kmer_cnt++;
         // first kmer found?  (base_match = 0?)
         if(base_match == 0)
         {
           base_match = KMER_SIZE;
         }
         else
         {
           int bm = min(KMER_SIZE, pos - last_pos); // min built into C
           base_match += bm;
         }
         last_pos = pos;
      }
  
    }
     
  }
  
  float pct = 0.0;
  pct = 100 * kmer_cnt / (float) ttl_kmer_cnt;
  
  
  printf("- contig base count: %d\n", base_cnt);
  printf("- matching kmer count: %d (%0.2f%%)\n", kmer_cnt, pct);
  
  pct = 100 * base_match / (float) base_cnt; // cast one to float so percent comes out correctly
  
  printf("- contig bases matched: %d (%0.2f%%)\n", base_match, pct);
  printf(SEP);
  
  return base_match;

}


/*
 *  load jmers into our tmp_map
 *  
 */
int load_kmers(string bases, map<unsigned long long int, int> &tmp_map)
{
  int max_kmers = 0;
  int kmer_cnt = 0; // number of matching kmers
  int base_cnt = 0;
  int len=0;
  
  unsigned long long int kmer_val = 0, rkmer_val = 0;
  char *cbases = const_cast<char*>(bases.c_str());
  int blen = bases.length();
  signed long long int mask = ~((-1LL) << 2*KMER_SIZE);
  
  // want at least 1 kmer with 31 kmers
  max_kmers = bases.length() - KMER_SIZE + 1;
  if (max_kmers <= 0)  { return 0; }
  
  
  // do hash map update, atomic operation
  
  
  for(int pos=0; pos < blen; pos++)
  {
    base_cnt++;
    
    int base = cbases[pos];
    long long int num=kmer_lookup[base];
    kmer_val = kmer_val << 2;
    kmer_val = kmer_val | num;
    kmer_val &= mask;
    
    
    //a < b ? a : b;
    num >= 0 ? len++ : len = 0;

   
    if(len>=KMER_SIZE)
    {
      rkmer_val = rcomp(kmer_val);
      kmer_val > rkmer_val ? kmer_val : rkmer_val;
      tmp_map[kmer_val]++;
      kmer_cnt++;
      
      // debugging
      if (debug)
      {
        cout << "**  kmer " << pos << ": " << bitset<64>(kmer_val) << " = " << kmer_val << endl;
        cout << "** rkmer " << pos << ": " << bitset<64>(rcomp(kmer_val)) << " = " << rcomp(kmer_val) << endl;
      }

    }
    
    
  }

  printf("- unique kmer count: %d/%d\n", tmp_map.size(), kmer_cnt);
  printf("- base count: %d\n", base_cnt);  
  return kmer_cnt;
}


long long int rcomp(long long int kmer){
  long long int rkmer=0;
  int cycles=KMER_SIZE;
  kmer=~kmer; //complement
  for(int i=0; i<cycles; i++){ //reverse
    rkmer=(rkmer<<2)|(kmer&3);
    kmer>>=2;
  }
  return rkmer;
}

/* kmer size = 5, using r.fa, q.fa
 *  q: ccAATGGGCGAccGTCACGccccc
 *  r: GGAATGGGCGAAGGTCACGGCGGC
 *       11111 = AATGG 58
 *        22222 = ATGGG 234
 *         33333 = TGGGC 937
 *          44444 = GGGCG 678
 *           55555 = GGCGA 664
 *                  66666 = GTCAC 721
 *                   77777 = TCACG 838
 *                  15 bases match
 */

