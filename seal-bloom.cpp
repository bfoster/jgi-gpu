/*
 * seal using bloom filter for hashing function
 *
 *  use data regions!
 */
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <bitset>
//#include <nvToolsExt.h>

using namespace std;


#define SEP "--------------------------------------------\n"
const bool debug = false; // show lots of debug or not

const int KMER_SIZE = 31;
const int THREAD_WIDTH = 5;
signed long long int kmer_lookup[128]; // = (signed char*); // malloc(128*sizeof(signed char));





class bloom {

	public:

	//Array of 32-bit values
	//These are saturating counters
	int* array;
	
	//Length of the array
	long long int size;

	//Number of hashes; more is slower but more accurate
	int hashes;

	//Constant table of random 64-bit numbers
	//Used as bitmasks for hashing
	long long int randomTable[8*256*16]; //up to 16 hashes are supported

	//Constant table of 8-bit reverse-complements
	long long int rcompTable[256];

	//Constructor
	//Size should ideally be prime
	bloom(int hashes_, long long int size_){
		hashes=hashes_;
		size=(size_|1LL);
		array=(int *)malloc(4*size);
		for(int i=0;i<size;i++) array[i]=0; 
		fillRandomTable();
		fillRcompTable();
		
	}

	//Perform a lookup of a kmer
	//This returns the minimum of N lookups
	//where N is the fixed hashes value.
	#pragma acc routine seq
	int lookup(long long int key){
		//cout << "Lookup " << key << endl;
		
		//key=canonize(key);
		int r=0x7FFFFFFF;
		for(int i=0; i<hashes && r>0; i++){
			long long int hashcode=hash(key, i);
			int value=lookupHashed(hashcode);
			r=min(r, value);
			//cout << "Loop " << i << ": value=" << value << ", r=" << r << endl;
		}
		return r;
	}
	
	//Perform lookup of a hashcode
	#pragma acc routine seq
	int lookupHashed(long long int hashcode){
		int index=hashcode%size;
		//cout << "hash = " << hashcode << ", index = " << index << ", array[index] = " << array[index] << endl;
		return array[index];
	}
	
	//Generate a 63-bit hashcode for an input 64-bit number
	#pragma acc routine seq
	long long int hash(long long int number0, int way){
		long long int number=number0;
		long long int code=number;
		int wayStart=8*256*way; //Start at a unique block of random numbers per hash
		for(int i=0; i<8; i++){
			//1-D notation for long[16][8][256] matrix
			int index=(int)((wayStart)+(i*256)+(number&0xFF));
			long long int mask=randomTable[index];
			//cout << "cell=" << (index) << "=" << mask << endl;
			code=code^mask;
			number>>=8;
		}
		//cout << bitset<64>(number0) << endl << bitset<64>(code) << endl;
		//cout << "way=" << way << ", wayStart=" << wayStart << endl << endl;
		return code&0x7FFFFFFFFFFFFFFFLL;
	}

	//increment the count for a kmer
	#pragma acc routine seq
	void increment(long long key){
		//cout << "Increment " << key << endl;
		//key=canonize(key);
		long long int hashcode=key;
		for(int i=0; i<hashes; i++){
			hashcode=hash(key, i);
			incrementHashed(hashcode);
		}
	}

	//This should be an atomic operation!
	#pragma acc routine seq 
	void incrementHashed(long long int hashcode){
		int index=hashcode%size;

		// prevent overflow
		int result;
		#pragma acc atomic capture
		{
			result = array[index]++;
		}
		
		if(result<0 || result==0x7FFFFFFF){
			result=0x7FFFFFFF;
			#pragma acc atomic write
			array[index]=result;
		}
		
		//cout << "hash = " << hashcode << ", index = " << index << ", array[index] = " << array[index] << endl;
	}

	//Random 64-bit number
	//Ripped from:
	//https://stackoverflow.com/questions/3665257/generate-random-long-number
	long long int lrand(){
		int r1=rand();
		int r2=rand();
		return (((long long int)r1)<<32) | ((long long int)(r2));
	}

	#pragma acc routine seq
	long long int min(long long int a, long long int b){
		return a < b ? a : b;
	}

	#pragma acc routine seq
	long long int max(long long int a, long long int b){
		return a > b ? a : b;
	}

	//Return a canonical version of this kmer or its reverse-complement
	#pragma acc routine seq
	long long int canonize(long long int kmer, int k){
		long long int rkmer=rcomp(kmer, k);
		return max(kmer, rkmer);
	}

	//Fast reverse-complement, 8 bits per cycle via lookup
	#pragma acc routine seq
	long long int rcomp(long long int kmer, int k){
		long long int rkmer=0;
		int cycles=k;
		kmer=~kmer; //complement
		for(int i=0; i<cycles; i++){ //reverse
			rkmer=(rkmer<<2)|(kmer&3);
			kmer>>=2;
		}
		return rkmer;
	}

    //Fast reverse-complement, 8 bits per cycle via lookup
    #pragma acc routine seq
    long long int rcompFast(long long int kmer, int k){
            long long int rkmer=0;
            int extra=k&3;

            //cout << bitset<64>(kmer) << endl;

            //kmer=~kmer; //No!  Bad dog!

            //First non-mult-of-4 bases
            for(int i=0; i<extra; i++){
                    rkmer=((rkmer<<2)|((~kmer)&3L));
                    kmer>>=2;
            }
            k-=extra;

            //K is now a multiple of 4
            for(int i=0; i<k; i+=4){
                    int tail=(int)(kmer&0xFF);
                    int rtail=rcompTable[tail];
                    rkmer=((rkmer<<8)|rtail);
                    kmer>>=8;
                    //cout << bitset<64>(kmer) << endl;
            }

            //cout << bitset<64>(rkmer) << endl << endl;
            exit;
            return rkmer;
    }

	
	//fill randomTable with random numbers at init
	void fillRandomTable(){
		int lim=8*256*16;
		for(int i=0; i<lim; i++){
			randomTable[i]=lrand();
		}
	}
		
	//fill rcompTable with 8-bit reverse-complements at init
	void fillRcompTable(){
		int lim=256;
		for(int i=0; i<lim; i++){
			rcompTable[i]=rcomp(i, 4);
		}
	}
	
	void saturation()
	{
		long s=0;
		for(int i=0;i<size;i++) {
			if (array[i] > 0) s++;
		}
		float saturation = s / ((float)size);
		float false_pos = 1;
		for(int i=0;i<hashes;i++) false_pos = false_pos * saturation;
		
		cout << "Saturation = " << s/((float)size) << endl;
		cout << " False pos = " << false_pos << endl;
	}
	

	
};


/*
 *
 *
 *
 */

// prototypes
int count_kmers(string bases, bloom &tmp_map);
int load_kmers(string bases, bloom &tmp_map);
int read_fasta(string ref_file, bloom &tmp_map, string);

/*
 *
 *
 *
 */


int main(int argc, char** argv) {

// lookup table
	kmer_lookup['A'] = 0;
	kmer_lookup['a'] = 0;
	kmer_lookup['C'] = 1;
	kmer_lookup['c'] = 1;
	kmer_lookup['G'] = 2;
	kmer_lookup['g'] = 2;
	kmer_lookup['T'] = 3;
	kmer_lookup['t'] = 3;
	kmer_lookup['N'] = -1;
	kmer_lookup['n'] = -1;
	

	//Create a Bloom filter - move to gpu and NEVER move out
	long BLOOM_SIZE = 40000000;
	bloom ref_bloom(3, BLOOM_SIZE); // 3 = number of hashes, 1000m = 1.4% sat
	#pragma acc data copyin(ref_bloom,ref_bloom.array[0:BLOOM_SIZE],kmer_lookup) //,bloom.hashes)
	{
		
		//signed long long int mask = ~((-1LL) << (2*KMER_SIZE));
		//cout << "kmer_size = " << KMER_SIZE << ", bit mask = " << std::bitset<64>(mask) << endl;
		//return 0;
		
		
		string ref_file, query_file;	
	
		//ref_file = "/ccsopen/proj/gen123/bbtools/data/GHNYP.fasta";
		//ref_file = "/ccsopen/proj/gen123/bbtools/data/GHHPH.fasta";
		//ref_file = "/ccsopen/proj/gen123/bbtools/data/GHSUS.fasta";
		//ref_file = "/global/projectb/scratch/brycef/gpu/GHNYA.fasta";
		//ref_file = "/ccsopen/proj/gen123/bbtools/data/GHNXX.fasta";
		ref_file = "/ccsopen/proj/gen123/bbtools/data/iso-ref.fasta";
		//ref_file = "r.fa";
		//ref_file = "/ccsopen/proj/gen123/bbtools/jgi-gpu/rand1.fa";
	
		query_file = "/ccsopen/proj/gen123/bbtools/data/GHHPH.fasta";
		query_file = "/ccsopen/proj/gen123/bbtools/data/iso-ref.fasta";
		//query_file = "/ccsopen/proj/gen123/bbtools/data/GHSUS.3k.fasta";
		//query_file = "/global/projectb/scratch/brycef/gpu/GHNYC.fasta";
		//query_file = "q.fa";
		//query_file = "/ccsopen/proj/gen123/bbtools/jgi-gpu/rand2.fa";
		
		
		int kmer_cnt = 0;
		kmer_cnt = read_fasta(ref_file, ref_bloom, "ref");
		
		// check saturation
		cout << endl;
		ref_bloom.saturation();
		// triple collision = saturation ** hashes;
		cout << endl;
		
		// load query file
		printf(SEP);
		printf("\n");
		int kmer_match = 0, base_match = 0;
		//base_match = read_fasta(query_file, ref_bloom, "query");
		kmer_match = read_fasta(query_file, ref_bloom, "query");
		
		
		
		cout << "kmer_match = " << kmer_match << ", kmer_cnt = " << kmer_cnt << endl;
		double pct = 0.0;
		pct = 100. * kmer_match / (double) kmer_cnt;
		
		printf("- kmer percent match = %.2f%%\n", pct);
		
		
		printf(SEP);
		
	}
	
	return 0;
}

/*
 * process reference
 * read in each contig
 * send to kmer-function to split into kmers and map into our reference hashmap
 *
 */
int read_fasta(string ref_file, bloom &tmp_map, string file_type="ref")
{
    //nvtxRangePushA("read_fasta");

    string line;
	string bases = "";
	string last_header = "";
	int base_cnt = 0, contig_cnt = 0;
	int kmer_cnt = 0, kmer_match = 0;
	
	cout << "Loading " << file_type << ": " <<  ref_file << "...\n";
	cout << "\n";
	
	ifstream myfile(ref_file.c_str());
	
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
	
		if(line.at(0) == '>') // single '' for char
		{
	
			if(!last_header.empty())
			{
	
				printf("contig: %d = %s\n", contig_cnt, last_header.c_str());
				//printf("- base count: %d\n", bases.length());
	
				// kmer-ize it
				// matching base count needed here
				if(file_type == "ref")
				{
					kmer_cnt += load_kmers(bases, tmp_map);
				}
				else
				{
					kmer_match += count_kmers(bases, tmp_map);
				}
	
			}

            //nvtxRangePushA("stringy goodness pt 2");
			contig_cnt++;
			base_cnt += bases.length();
		
			bases = "";
			last_header = line;
			//nvtxRangePop();
		}
		else
		{

            //nvtxRangePushA("stringy goodness");
			//bases = bases + line;
			base.append(line);
            //nvtxRangePop();
		}

		}
		myfile.close();
	}
	else cout << "Unable to open file" << endl;   
	
	// handle last of bases
	base_cnt += bases.length();
	
	
	printf("last contig: %d = %s\n", contig_cnt, last_header.c_str());
	//printf("- base count: %d\n", bases.length());

	printf("\n");
	printf("- total contig count: %d\n", contig_cnt);
	printf("-   total base count: %d\n", base_cnt);

	
	if(file_type == "ref")
	{
		
		kmer_cnt += load_kmers(bases, tmp_map);
		cout << "== " << kmer_cnt << endl;
        //nvtxRangePop();
		return kmer_cnt;
	}
	
	
	
	kmer_match += count_kmers(bases, tmp_map);
	cout << "== " << kmer_match << endl;

	//nvtxRangePop();
	return kmer_match;
}

/*
 *  Count how many kmers from bases are in tmp_map
 *  
 *  
 */
int count_kmers(string bases, bloom &tmp_map)
{
    //nvtxRangePushA("count_kmers");
	int max_kmers = 0;
	int base_cnt = 0; // number of matching bases (if kmer exists in ref)
	int blen=bases.length();
	int last_pos = 0;
	int len=0; // every thread has private copy of length
	signed long long int mask = ~((-1LL) << 2*KMER_SIZE);
	char *cbases = const_cast<char*>(bases.c_str());
	int matches_cnt = 0; // number of matching kmers
	int kmer_cnt = 0; // total kmers found
	int base_match = 0;
	long long int kmer_val = 0;
	
	// want at least 1 kmer with 31 kmers
	max_kmers = bases.length() - KMER_SIZE + 1;
	if (max_kmers <= 0)	{ nvtxRangePop(); return 0;	}

    
	#pragma acc parallel loop reduction(+:matches_cnt,base_match,kmer_cnt,base_cnt) copyin(cbases[0:blen])
    for(int thread=0; thread < ((blen +9) / THREAD_WIDTH); thread++)
    {
        long long int kmer_val = 0;
        int len=0; // every thread has private copy of length
        int matches_thread = 0; // number of matching kmers
        int base_match_thread = 0;
        int kmer_cnt_thread = 0;
        int last_pos_thread = 0;
        int base_cnt_thread = 0;
		
		#pragma acc loop seq
        for(int pos=thread*THREAD_WIDTH; pos < blen && pos < (thread+1) * THREAD_WIDTH + KMER_SIZE - 1; pos++)
        {
        
            //base_cnt_thread++;
            int base = cbases[pos];
            long long int num=kmer_lookup[base];
            kmer_val = kmer_val << 2;
            kmer_val = kmer_val | num;
            kmer_val &= mask;
	
			num >= 0 ? len++ : len = 0;
	
            if(len>=KMER_SIZE)
            {
                long long int key=tmp_map.canonize(kmer_val, KMER_SIZE);
                kmer_cnt_thread++;
                if(tmp_map.lookup(key) > 0)
                {
                    matches_thread++;
                    // first kmer found?  (base_match = 0?)
                    if(base_match_thread == 0)
                    {
                        base_match_thread = KMER_SIZE;
                    }
                    else
                    {
                        int bm = min(KMER_SIZE, pos - last_pos_thread); // min built into C
                        base_match_thread += bm;
                    }
                    last_pos_thread = pos;
                }
	
		    } 
        }	

        kmer_cnt += kmer_cnt_thread;
        matches_cnt += matches_thread;
        //base_cnt += base_cnt_thread;
        base_match += base_match_thread;
	}	
	
	float pct = 0.0;
	
	//printf("-   contig base count: %d\n", base_cnt);
	pct = 100 * matches_cnt / (float) kmer_cnt;
	printf("- matching kmer count: %d (%0.2f%%)\n", matches_cnt, pct);
	
	base_cnt = bases.length();
	pct = 100 * base_match / (float) base_cnt; // cast one to float so percent comes out correctly
	printf("- contig bases matched: %d (%0.2f%%)\n", base_match, pct);
	
	printf(SEP);
	//cout << "== " << matches_cnt << "|" << kmer_cnt << endl;
    //nvtxRangePop();
	return matches_cnt;

}


/*
 *  load kmers into ye ol' bloom fylter
 *  
 */
int load_kmers(string bases, bloom &tmp_map)
{
    //nvtxRangePushA("load_kmers");
	int max_kmers = 0;
	int kmer_cnt = 0; // number of matching kmers
	int base_cnt = 0;
	int len=0;
	
	
	char *cbases = const_cast<char*>(bases.c_str());
	int blen = bases.length();
	signed long long int mask = ~((-1LL) << 2*KMER_SIZE);
	
	// want at least 1 kmer with 31 kmers
	max_kmers = bases.length() - KMER_SIZE + 1;
	if (max_kmers <= 0)	{ nvtxRangePop(); return 0;	}
	
	
	// do hash map update, atomic operation

	#pragma acc parallel loop reduction(+:kmer_cnt,base_cnt) copyin(cbases[0:blen])
    for(int thread=0; thread < ((blen +9) / THREAD_WIDTH); thread++)
    {
        long long int kmer_val = 0;
        int len=0; // every thread has private copy of length
        int kmer_cnt_thread = 0;
		int base_cnt_thread = 0;
		
		#pragma acc loop seq
        for(int pos=thread*THREAD_WIDTH; pos < blen && pos < (thread+1) * THREAD_WIDTH + KMER_SIZE - 1; pos++)
        {
        	

			base_cnt_thread++;
	
			int base = cbases[pos];
			long long int num=kmer_lookup[base];
			kmer_val = kmer_val << 2;
			kmer_val = kmer_val | num;
			kmer_val &= mask;
		
			num >= 0 ? len++ : len = 0;
	
			if(len>=KMER_SIZE)
			{
				long long int key=tmp_map.canonize(kmer_val, KMER_SIZE);
				tmp_map.increment(key);
				kmer_cnt_thread++;		
			}
		}
		
		kmer_cnt += kmer_cnt_thread;
		base_cnt += base_cnt_thread;
	}
	
	printf("- kmer count: %d\n", kmer_cnt); 
	printf("- base count: %d\n", base_cnt);  

    //nvtxRangePop();
	return kmer_cnt;

}

